package modulo4;
import java.util.Scanner;
public class Ejercicio9 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion los valores son: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer competidor");
		float PrimerCompetidor=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo competidor");
		float SegundoCompetidor=scan.nextFloat();
		
		if (PrimerCompetidor == SegundoCompetidor)
			System.out.println("Empate");
		else
		{
			if (PrimerCompetidor == 0 && SegundoCompetidor == 2)
				System.out.println("Gano competidor numero uno");
			else if (PrimerCompetidor == 0 && SegundoCompetidor == 1)
				System.out.println("Gano competidor numero dos");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 0)
				System.out.println("Gano competidor numero uno");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 2)
				System.out.println("Gano competidor numero dos");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 0)
				System.out.println("Gano competidor numero dos");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 1)
				System.out.println("Gano competidor numero uno");
		}
		scan=null;

	}

}
