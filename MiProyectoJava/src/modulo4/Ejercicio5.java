package modulo4;
import java.util.Scanner;


public class Ejercicio5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el puesto en un valor numerico");
		int Puesto =scan.nextInt();
		
		if(Puesto == 1)
			System.out.println("Se lleva la medalla de oro, Titan");
		else if(Puesto == 2)
			System.out.println("Se lleva la medalla de plata, nadie se acuerda del segundo");
		else if(Puesto == 3)
			System.out.println("Se lleva la medalla de bronce, al lobby");
		else
			System.out.println("Segui participando");
		
		scan=null;

	}

}
