package modulo4;
import java.util.Scanner;


public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes");
		String grupo = scan.nextLine();
		
		if (grupo.equals("Abril") || grupo.equals("junio") || grupo.equals("septiembre") || grupo.equals("noviembre"))
			System.out.println("El mes tiene 30 dias");
		else if (grupo.equals("enero") || grupo.equals("marzo") || grupo.equals("julio") || grupo.equals("agosto") || grupo.equals("octubre") || grupo.equals("diciembre"))
			System.out.println("El mes tiene 31 dias");
		else if (grupo.equals("febrero"))
			System.out.println("El mes tiene 28 dias");
		else
			System.out.println("No ingreso un mes correcto");
		
		scan=null;


	}

}
