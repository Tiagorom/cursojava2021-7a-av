package modulo2;

public class Ejercicio4 {

	public static void main(String[] args) {
		//Ejercicio 4
		
		byte b=10;
		short s=20;
		int i=30;
		long l=40;
		
		byte sumabb =  (byte) (b+b);
		short sumabs = (short) (b+s);
		int sumabi = b+i;
		int sumaii = i+i;
		long sumasl = s+l;
		
		System.out.println(sumabb);
		System.out.println(sumabs);
		System.out.println(sumabi);
		System.out.println(sumaii);
		System.out.println(sumasl);
		
		//Ejercicio 5
		
		l=s;
		b=(byte)s;
		System.out.println("b=s error de compilacion");
		l=i;
		b=(byte)i;
		System.out.println("b=i error de compilacion");
		s=(short)i;
		System.out.println("s=i error de compilacion");

	}

}
